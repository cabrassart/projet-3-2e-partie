# coding: utf8

'''
Mini-projet "Choipeaux" première partie avec
les réponses qui s'affichent dans la console.

Auteurs: Clémence ABRASSART, Andrea BASSI, Sacha BECOTE

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
11/04/2022

'''
from math import sqrt

# Définition des fonctions
def distance(eleve1, eleve2):
    '''
    Calcule la distance entre deux élèves.
    Entrée: deux dictionnaires
    Sortie: un flottant
    '''
    return sqrt((eleve1['Good'] - eleve2['Good'])**2 + (eleve1['Courage'] -
                eleve2['Courage'])**2 + (eleve1['Ambition'] -
                eleve2['Ambition'])**2 + (eleve1['Intelligence'] -
                eleve2['Intelligence'])**2)


def combinaisons(table, eleve):
    '''
    Crée une liste des distances avec cet élève.
    Entrée: une table et un dictionnaire
    Sortie: une liste de listes (le personnage et sa distance à l'élève)
    '''
    liste_dist = []
    for element in table:
        liste_dist.append([element, distance(element, eleve)])
    return liste_dist


def plus_proches_voisins(k, liste):
    '''
    Détermine les k plus proches voisins.
    Entrée: un entier et une table
    Sortie: une table
    '''
    voisins = []
    liste.sort(key=lambda x: x[1])
    for i in range(k):
        voisins.append([liste[i][0]['Name'], liste[i][0]['House']])
    return voisins


def maison(liste_voisins):
    '''
    Détermine la maison du nouvel élève.
    Entrée: une table
    Sortie: une chaine de charactères
    '''
    maisons = {"Hufflepuff": 0, "Ravenclaw": 0, 
               "Gryffindor": 0, "Slytherin": 0}
    for perso in liste_voisins:
        maisons[perso[1]] += 1
    la_maison = list(maisons.items())
    la_maison.sort(key=lambda x: x[1], reverse=True)
    return la_maison[0][0]

def moyenne(tab):
    '''
    Créé le profil en faisant la moyenne 
    Entrée: une table
    Sortie: un dictionnnaire
    '''
    profil = {}
    for key in tab[0].keys():
        total = 0
        for element in tab:
            total += element[key]
        profil[key] = total / len(tab)
    return profil
    
def profile(tab):
    dico = {}
    for key in tab[0].keys():
        liste = []
        for element in tab:
            liste.append(element[key])
        liste.sort()
        dico[key] = liste[-3]
    return dico
 
