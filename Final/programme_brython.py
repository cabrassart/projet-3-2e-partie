from browser import document, html
from python import*
# fontions brython
document <= html.BUTTON("Commencer le quizz", id='debut')

def start(ev):
    document["debut"].style.display = 'none'
    document <= html.H2(question_tab[0]["Questions"], id="questions")
    for j in range(1,5):
        document <= html.P(html.BUTTON(question_tab[0]["Réponse " + str(j)], id='rep'+str(j), value=j))
        document['rep'+str(j)].bind("click", reponses)

document["debut"].bind("click", start)

num_ques = 0
reponse = []
def reponses(event):
    global num_ques
    # on récupère les valeurs
    dico = {}
    dico["Good"] = question_tab[num_ques]["good " + str(event.target.value)]
    dico["Courage"] = question_tab[num_ques]["courage " + str(event.target.value)]
    dico["Ambition"] = question_tab[num_ques]["ambition " + str(event.target.value)]
    dico["Intelligence"] = question_tab[num_ques]["intelligence " + str(event.target.value)]
    reponse.append(dico)
    if num_ques >= len(question_tab) - 1:
        for i in range(1,5):
    	    document["rep" + str(i)].style.display = 'none'
        perso = profile(reponse)
        ses_voisins = plus_proches_voisins(5, combinaisons(table_carac, perso))
        sa_maison = maison(ses_voisins)
        document["questions"].textContent = "Le choipeau vous affecte à la maison :"
        document <= html.H1(sa_maison.upper(), id="maison")
        document <= html.IMG(src=sa_maison+".jpg")
        document <= html.P("Les personnages qui vous ressemblent sont :")
        document <= html.TABLE(html.TR(html.TH("Nom") + html.TH("Maison")) + (html.TR(html.TD(ses_voisins[i][0]) + html.TD(ses_voisins[i][1])) for i in range(5)))
        
    else:
        # passage à la question suivante
        num_ques += 1
        document["questions"].textContent = question_tab[num_ques]["Questions"]
        for i in range(1,5):
            document["rep" + str(i)].textContent = question_tab[num_ques]["Réponse " + str(i)]
            
	


# import des fichiers csv
question_tab = []
with open("questions.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        question_tab.append(dico)

for element in question_tab:
    for i in range(1,5):
        element["courage " + str(i)] = int(element["courage " + str(i)])
        element["ambition " + str(i)] = int(element["ambition " + str(i)])
        element["intelligence " + str(i)] = int(element["intelligence " + str(i)])
        element["good " + str(i)] = int(element["good " + str(i)])
        

table_perso = []
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        table_perso.append(dico)

table_carac = []
with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        table_carac.append(dico)

for element in table_carac:
    element['Courage'] = int(element['Courage'])
    element['Ambition'] = int(element['Ambition'])
    element['Intelligence'] = int(element['Intelligence'])
    element['Good'] = int(element['Good'])
    for perso in table_perso:
        if element['Name'] == perso['Name']:
            element['House'] = perso['House']
