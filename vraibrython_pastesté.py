from browser import document, html
# création de la table avec les questions
question_tab = []
with open("C:/Users/cleme/Documents/103/nsi/3.2/questions.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        question_tab.append(dico)

for element in question_tab:
    for i in range(1,4):
        element["courage " + str(i)] = int(element["courage " + str(i)])
        element["ambition " + str(i)] = int(element["ambition " + str(i)])
        element["intelligence " + str(i)] = int(element["intelligence " + str(i)])
        element["good " + str(i)] = int(element["good " + str(i)])


# definition des fonctions brython
document <= html.BUTTON("Commencer le quizz", id='start')

def start(ev):
    document["start"].style.display = 'none'
    document <= html.H3("question", id="question")
    for i in range(1,4):
        document <= html.P(html.BUTTON("i", id='rep' + str(i), value=i))
   
document["start"].bind("click", start)



num_ques = 0
def next_ques(event):
    global num_ques
    document["question"].textContent = question_tab[num_ques][0]
    for i in range(1,4):
        document["rep" + str(i)].textContent = question_tab[num_ques][i]
    num_ques += 1

for i in range(1,4):
    document['rep' + str(i)].bind("click", next_ques)
    
    
tab_ans = []
def carac(event):
    dico = {}
    dico["Good"]= question_tab[num_ques]["good "+str(event.target.value)]
    dico["Courage"]= question_tab[num_ques]["courage "+str(event.target.value)]
    dico["Ambition"]= question_tab[num_ques]["ambition "+str(event.target.value)]
    dico["Intelligence"]= question_tab[num_ques]["intelligence "+str(event.target.value)]
    tab_ans.append(dico)
    
for i in range(1,4):
    document['rep' + str(i)].bind("click", carac)
    
    
def moyenne(table):
    profil = {}
    for key in tab_ans.keys():
        somme = 0
        for element in tab_ans:
            somme += element[key]
        profil[key] = somme / len(tab_ans)
    return profil